const convertirNroADig = require('../src/conversorADigital');
const convertirNumero = require('../src/convertirNumero');

describe('Convertir a Digital', () => {
  test('Convierte el 9 ', () => {
    expect(convertirNumero(9)).toMatch('_ \n|_|\n _|\n ');
  });

  console.log('convierte el 9\n' + convertirNumero(9));

  test('Convierte el 123 ', () => {
    expect(convertirNumero(123)).toMatch(
      '   \n  |\n  |\n \n  _ \n _|\n|_ \n \n _ \n _|\n _|\n '
    );
  });

  console.log('convierte el 123\n' + convertirNumero(123));
});
