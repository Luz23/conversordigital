function convertirNroADigital(num) {
  switch (num) {
    case '0':
      return ' _ \n| |\n|_|\n ';

    case '1':
      return '   \n  |\n  |\n ';

    case '2':
      return '\n  _ \n _|\n|_ \n ';

    case '3':
      return '\n _ \n _|\n _|\n ';

    case '4':
      return '   \n|_|\n  |\n ';

    case '5':
      return '\n _ \n|_ \n _|\n ';

    case '6':
      return '\n _ \n|_ \n|_|\n ';

    case '7':
      return '\n _ \n  |\n  |\n';

    case '8':
      return '\n _ \n|_|\n|_|\n ';

    case '9':
      return '_ \n|_|\n _|\n ';
  }

  return '';
}

module.exports = convertirNroADigital;
