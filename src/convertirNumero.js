const convertirNroADig = require('./conversorADigital');

function convertirNumero(num) {
  var numero = num.toString();
  var largoNum = num.toString().length;
  var cadena = '';
  var paso;
  for (paso = 0; paso < largoNum; paso++) {
    cadena += convertirNroADig(numero.charAt(paso));
  }
  return cadena;
}

module.exports = convertirNumero;
